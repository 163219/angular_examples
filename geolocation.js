var GeoApp = angular.module("GeoApp", ["uiGmapgoogle-maps"]);

var GeoCtrl = function($scope) {

    var vm = this;
    
    vm.position = { 
        latitude: 0, longitude: 0,
        zoom: 16,
        id: 0,
        options: {
            draggable: false
        }
    };

    vm.getLocation = function() {
       navigator.geolocation.getCurrentPosition(function(pos) {
           console.info("In pos")
           $scope.$apply(function() {
               vm.position.latitude = pos.coords.latitude;
               vm.position.longitude = pos.coords.longitude;
           });
       });
    }
}

GeoApp.controller("GeoCtrl", ["$scope", GeoCtrl]);