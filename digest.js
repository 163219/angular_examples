(function() {
    
    var CounterApp = angular.module("CounterApp", []);
    
    var CounterCtrl = function($scope) {
        var vm = this;
        vm.counter = 0;
        
        //Brower function
        setInterval(function() {
            //Start digest cycle manually
            $scope.$apply(function() {
                console.info("vm.counter = %d", ++vm.counter);
            });
        }, 1000);
    }
    
    CounterApp.controller("CounterCtrl", 
        ["$scope", CounterCtrl]);
})();
