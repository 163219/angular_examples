(function() {
    var ModalApp = angular.module("ModalApp", []);
    var ModalCtrl = function(ModalSvc) {
        var vm = this;
        vm.fruits = ["Apple", "Orange", "Pear", "Grapes", "Pineapple"];
        vm.show = function(f) {
            ModalSvc.currentValue = f;
			//Use jQuery to display the modal
            $("#fruit-modal").modal("show");
        }
    }
    var FruitModalCtrl = function($scope, ModalSvc) {
        var vm = this;
        console.info("fruit ctrl: " + ModalSvc.currentValue);
		//Set a listener for shown.bs.modal event. Event will be fired
		//when modal is shown. 
        $("#fruit-modal").on("shown.bs.modal", function() {
			//Once modal is shown, start a digest cycle to update the values in modal
            $scope.$apply(function() {
                vm.fruit = ModalSvc.currentValue;
            });
        });
    };
    //Service
    var ModalSvc = function() {
        this.curentValue = "";
    }
    ModalApp.service("ModalSvc", [ModalSvc]);
    
    ModalApp.controller("ModalCtrl", ["ModalSvc", ModalCtrl]);
    ModalApp.controller("FruitModalCtrl", ["$scope", "ModalSvc", FruitModalCtrl]);
})();
